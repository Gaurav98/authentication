package com.nearpe.authentication.service;

import com.nearpe.authentication.model.User;
import com.nearpe.authentication.repositories.RoleRepository;
import com.nearpe.authentication.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    ///////// Constructor /////////

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    ///////// Functions to be performed in Repository /////////

    //Create
    public User createUser(User user){
        return userRepository.save(user);
    }

    //Finding
    public List<User> findAll(){
        return userRepository.findAll();
    }

    public Optional<User> findUserById(Integer id) {
        if(userRepository.findById(id).isEmpty())
            return Optional.empty();

            return userRepository.findById(id);
    }

    public User findUserByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public User findByUsername(String userName) {return userRepository.findByUserName(userName);}

    //Update
    public User updateById(Integer id) {
        return  userRepository.save(id);
    }

    public User updateUser(User user) { return userRepository.save(user); }

    //Delete
    public void deleteUserById(Integer id) {
        userRepository.deleteById(id);
    }

}
