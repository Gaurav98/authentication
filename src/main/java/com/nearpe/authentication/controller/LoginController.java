package com.nearpe.authentication.controller;

import com.nearpe.authentication.Exception.UserNotFoundException;
import com.nearpe.authentication.model.User;
import com.nearpe.authentication.repositories.UserRepository;
import com.nearpe.authentication.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
public class LoginController {

    @Autowired
    private UserService userService;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    ///////// Constructor /////////

    public LoginController(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    ///////// Get Mapping /////////

    @GetMapping({"/user/login"})
    public String login(){
        return "loginPage";
    }

    @GetMapping("/user/{id}")
    public Optional<User> getUsers(@PathVariable Integer id){
        return userService.findUserById(id);
    }

    @GetMapping("/getall")
    public List<User> getAll(){
        return userService.findAll();
    }

    ///////// Post Mapping /////////
//
//    @PostMapping("/user/login")
//    public Optional<User> loginUser(@RequestBody User user){
//        log.info(user.toString());
//        User getUser = userService.findUserByEmail(user.getEmail());
//        if(userService.findUserById(getUser.getId()).isEmpty())
//            throw new UserNotFoundException(user.getId());
//
//        return userService.findUserById(getUser.getId());
//    }


    @PostMapping("/registration")
    public User createUser(@RequestBody User user){
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userService.createUser(user);
    }

    ///////// Delete Mapping /////////

    @DeleteMapping("/delete/{id}")
    void deleteUser(@PathVariable Integer id){
       userService.deleteUserById(id);
    }

    ///////// Put Mapping /////////

    @PutMapping("/user/{id}")
    Object updateUser(@RequestBody User user,@PathVariable Integer id){

        userService.deleteUserById(id);
        Optional<User> updateUser = Optional.ofNullable(userService.findUserByEmail(user.getEmail()));
        if(updateUser.isPresent()){
            return userService.createUser(user);
        }
        User getUser = new User();
        getUser.setUserName(user.getUserName());
        getUser.setEmail(user.getEmail());
        getUser.setId(id);
        getUser.setRole(user.getRole());

        return userService.createUser(getUser);
    }

    /////////
}
