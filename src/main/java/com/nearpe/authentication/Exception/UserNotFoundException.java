package com.nearpe.authentication.Exception;

public class UserNotFoundException extends RuntimeException{

    public UserNotFoundException(int id){
        super("Could not find user with ID: "+id);
    }
}
