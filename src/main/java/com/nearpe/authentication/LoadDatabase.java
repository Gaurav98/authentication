package com.nearpe.authentication;

import com.nearpe.authentication.model.User;
import com.nearpe.authentication.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;


import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

///////// Preconfigured Database /////////

@Slf4j
@Configuration
public class LoadDatabase {

    ///////// Table database initiated /////////

    @Bean
    CommandLineRunner initDatabase(UserRepository repository) {
        return args -> {
            User user = new User();
            user.setId(1);
            user.setUserName("Gaurav");
            user.setEmail("gauravgkm5@gmail.com");
            user.setPassword("Gaurav");
            log.info("Preloading " + repository.save(user));

            user.setId(2);
            user.setUserName("Ayush");
            user.setEmail("waliaayush@gmail.com");
            user.setPassword("walia");
            log.info("Preloading " + repository.save(user));

            user.setId(3);
            user.setUserName("Alind");
            user.setEmail("alindKumar@gmail.com");
            user.setPassword("alind");
            log.info("Preloading " + repository.save(user));

            user.setId(4);
            user.setUserName("Ayush");
            user.setEmail("ayushgupta@gmail.com");
            user.setPassword("guptaji");
            log.info("Preloading " + repository.save(user));

        };
    }

}
