package com.nearpe.authentication.repositories;

import com.nearpe.authentication.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Id;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

        ///////// Crud Operations /////////

        User findByEmail(String email);

        User findById(Id id);

        User findByUserName(String userName);

        List<User> findAll();

        User save(Integer id);

        User save(User user);

        void deleteById(Integer id);


}
